const express = require('express');
const router = express.Router();
const User = require('../model/model').User;

router.post('/create', async function(req, res, next) {
  let data = req.body;
  console.log('data' , data);
  let user = await User.create(data);
  res.send({
    success: true,
    message: 'user created',
    data:user
  });
});

module.exports = router;
