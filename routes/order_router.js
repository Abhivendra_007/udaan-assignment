const express = require('express');
const router = express.Router();
const User = require('../model/model').User;
const Cab = require('../model/model').Cab;
const Order = require('../model/model').Order;

router.get('/:userId', async function(req, res, next) {
    let userId = req.params.userId;
    let order = await Order.find({userId:userId}).sort({createdAt:-1})
    res.send({
        success: true,
        message: "old rides fetched",
        data:order
      });
});
router.post('/create/:userId', async function(req, res, next) {
    let userId = req.params.userId;
    let data = req.body;
    let user = await User.findOne({_id:userId});
    let latlng = user.currentLocation;
    let cab = await Cabs.findOne({$geoNear:{near:{type:"Point",coordinates:user.currentLocation},distanceField: "currentLocation",$maxDistance:150000,spherical: true}}); //nearest cab
    if (!cab){
        res.send({
            success: false,
            message: "No cab found!"
          });
    }
    let orderObj = {
        cabId: cab._id,
        userId : userId,
        status: "new",
        startLocation: user.currentLocation,
        endLocation: data.endLocation
    }
    let order = await Order.create(orderObj)
    res.send({
        success: true,
        message: "order created!",
        data:order
      });
});

router.post('/start/:orderId', function(req, res, next) {
    res.send('order created');
});

router.post('end/:orderId', function(req, res, next) {
  res.send('old rides');
});

module.exports = router;
