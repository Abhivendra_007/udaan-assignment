const express = require('express');
const router = express.Router();
const Cab = require('../model/model').Cab;

router.post('/create', async function(req, res, next) {
  let data = req.body;
  console.log('data' , data);
  let cab = await Cab.create(data);
  res.send({
    success: true,
    message: 'cab created',
    data:cab
  });
});

router.post('/update-location/:cabId', async function(req, res, next) {
  let data = req.body;
  let cabId = req.params.cabId;
  console.log('data' , data);
  let cab = await Cab.update({_id:cabId}, {$set:{currentLocation:data}});
  res.send({
    success: true,
    message: 'cab location updated',
    data:cab
  });
});

router.post('/update-availability/:cabId', async function(req, res, next) {
  let cabId = req.params.cabId;
  let data = req.body;
  let cab = await Cab.update({_id:cabId}, {$set:{isAvailable:data.isAvailable}});
  res.send({
    success: true,
    message: 'cab avalability updated',
    data:cab
  });
});

module.exports = router;
