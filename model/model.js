const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true, useUnifiedTopology: true});
const Schema = mongoose.Schema;

const locationSchema = new Schema({
    type: [Number],
    default: [0, 0],
});
const userSchema = new Schema({
    phone:{ type: String, index: true },
    currentLocation: locationSchema,
    name: String,
    createdAt: { type: Date, default: Date.now },
});
const driverSchema = new Schema({
    name:String,
    phone: { type: String, index: true },
    ratting: Number,
});
const cabSchema = new Schema({
    driver: driverSchema,
    currentLocation: locationSchema,
    registrationDetail: String,
    isAvailable: Boolean,
    createdAt: { type: Date, default: Date.now },
});
const orderSchema = new Schema({
    cabId: { type: String, index: true },
    userId : { type: String, index: true },
    status: { 
        type: String,    
        enum : ['new','started', 'ended'], 
        default: 'new' 
    },
    startLocation: locationSchema,
    endLocation: locationSchema,
    createdAt: { type: Date, default: Date.now },
});
cabSchema.index({currentLocation: '2dsphere'});
userSchema.index({currentLocation: '2dsphere'});

const User = mongoose.model('user', userSchema);
const Cab = mongoose.model('cab', cabSchema);

const Order = mongoose.model('order', orderSchema);

const Models = {
    User: User,
    Cab: Cab,
    Order: Order
};

module.exports = Models;
